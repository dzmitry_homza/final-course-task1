from requests_api_gorest import User
from requests_api_gorest import get_var_json
import pytest


admin = User()
id = admin.create_user_take_id()


@pytest.mark.positive
def test_created_user():
    response = admin.my_request(get_var_json("get"), id=id)
    assert response.json()["data"]["name"] == get_var_json("name1")


@pytest.mark.positive
def test_get_users_list():
    response = admin.my_request(get_var_json("get"))
    assert response.json()["code"] == 200


@pytest.mark.positive
def test_update_user_put():
    name2 = get_var_json("name2")
    email2 = get_var_json("email2")
    token = get_var_json("token")
    json = admin.payload(name2, email2)
    response = admin.my_request(get_var_json("put"), id=id, json=json,
                                headers=token)
    assert response.json()["data"]["name"] == get_var_json("name2")


@pytest.mark.positive
def test_update_user_patch():
    name3 = get_var_json("name3")
    email3 = get_var_json("email3")
    token = get_var_json("token")
    json = admin.payload(name3, email3)
    response = admin.my_request(get_var_json("patch"), id=id, json=json,
                                headers=token)
    assert response.json()["data"]["name"] == get_var_json("name3")


@pytest.mark.negative
@pytest.mark.xfail(reason="найден баг - изменение данный в чужих аккаунтах",
                   strict=True)
def test_negative_put_with_not_my_id():
    name4 = get_var_json("name4")
    email4 = get_var_json("email4")
    token = get_var_json("bad_token")
    json = admin.payload(name4, email4)
    if admin.my_request(get_var_json("get"), id=14, json=json, headers=token):
        response = admin.my_request(get_var_json("put"), id=14, json=json,
                                    headers=token)
    assert response.json()["code"] == 401


@pytest.mark.negative
@pytest.mark.xfail(reason="найден баг - удаление чужих аккаунтов",
                   strict=True)
def test_delete_user_with_not_my_id():
    token = get_var_json("token")
    response = admin.my_request(get_var_json("delete"), id=14,
                                headers=token)
    assert response.json()["code"] == 401


@pytest.mark.negative
def test_negative_update_user_with_bad_token():
    name4 = get_var_json("name4")
    email4 = get_var_json("email4")
    token = get_var_json("bad_token")
    json = admin.payload(name4, email4)
    response = admin.my_request(get_var_json("put"), id=id, json=json,
                                headers=token)
    assert response.json()["data"]["message"] == "Authentication failed"


@pytest.mark.positive
def test_delete_user():
    token = get_var_json("token")
    response = admin.my_request(get_var_json("delete"), id=id,
                                headers=token)
    assert response.json()["code"] == 204
