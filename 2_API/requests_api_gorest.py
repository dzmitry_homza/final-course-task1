import requests
import json


def get_var_json(key):
    with open("conf.json", "r") as f:
        dict_j = json.load(f)
        return dict_j[key]


class User:
    def __init__(self):
        self.base_url = get_var_json("base_url")

    @staticmethod
    def create_user_take_id():
        admin = User()
        name = get_var_json("name1")
        email = get_var_json("email1")
        token = get_var_json("token")
        response = admin.my_request(get_var_json("post"),
                                    json=User.payload(name, email),
                                    headers=token)
        return response.json()["data"]["id"]

    @staticmethod
    def payload(name, email):
        payload = {
            "name": name,
            "gender": "Male",
            "email": email,
            "status": "Active"
        }
        return payload

    def my_request(self, method, id="", json=None, data=None, params=None,
                   headers=None):
        url = f"{self.base_url}/{id}"
        print(f"REQUEST URL: {url}")
        print(f"NAME OF METHOD: {method}")
        print(f"BODY: {json}")
        try:
            response = requests.request(method=method, url=url,
                                        json=json, data=data, params=params,
                                        headers=headers)
            print(f"STATUS CODE: {response.status_code}")
            print(f"RESPONSE HEADERS: {response.headers}")
            print(f"RESPONSE: {response.text}")
            assert response.status_code == 200
            return response

        except requests.exceptions.RequestException as e:
            raise SystemExit(e)


if __name__ == "__main__":
    Bob = User()
    print(get_var_json("base_url"))
    print(get_var_json("token"))
    name = get_var_json("name1")
    email = get_var_json("email1")
    token = get_var_json("token")
    response = Bob.my_request(get_var_json("post"),
                              json=User.payload(name, email), headers=token)
