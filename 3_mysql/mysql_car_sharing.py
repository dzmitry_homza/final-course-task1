import docker
import mysql.connector as mysql


client = docker.from_env()

# START MYSQL:--------
container = client.containers.get('0398bc3a14b5')
container.start()
# ---------------------------

# CONNECT TO MYSQL:-------
database = mysql.connect(host="localhost",
                         user="dima",
                         passwd="123",
                         port=3306
                         )
# --------------------------------
# CREATE DATABASE car_sharing: ---
cursor = database.cursor()
cursor.execute("CREATE DATABASE car_sharing")
# --------------------------------
#
# CONNECT TO DATABASE car_sharing:-------
database = mysql.connect(host="localhost",
                         user="dima",
                         passwd="123",
                         port=3306,
                         database="car_sharing"
                         )
# -----------------------------------
# CREATE TABLE cars with columns: ---
cursor = database.cursor()
cursor.execute('CREATE TABLE cars ('
               ' id INT(11) NOT NULL AUTO_INCREMENT PRIMARY KEY,'
               ' type_car VARCHAR(30),'
               ' description VARCHAR(30),'
               ' car_number VARCHAR(30),'
               ' day_price INT,'
               ' transmission VARCHAR(30), '
               ' mileage INT)')
database.commit()
# -----------------------------
# ADD DATA TO cars: ----------
cursor = database.cursor()
query = "INSERT INTO cars (" \
        "type_car, description, car_number," \
        " day_price, transmission, mileage) VALUES (%s, %s, %s, %s, %s, %s)"
values = [
    ("jeep", "reliable", "88-22", 1000, "A", 39825),
    ("truck", "powerful", "99-55", 699, "M", 784510),
    ("sport", "fast", "20-20", 250, "M", 1300)
]
cursor.executemany(query, values)
database.commit()
print(cursor.rowcount, "records inserted")

# !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
# CREATE TABLE customers with columns: ---
cursor = database.cursor()
cursor.execute('CREATE TABLE customers ('
               ' id INT(11) NOT NULL AUTO_INCREMENT PRIMARY KEY,'
               ' name VARCHAR(30),'
               ' tel INT,'
               ' address VARCHAR(100))')
database.commit()
# -----------------------------
# ADD DATA TO customers: ----------
cursor = database.cursor()
query = "INSERT INTO customers (name, tel, address) VALUES (%s, %s, %s)"
values = [
    ("Dima", 291592514, "Belarus,Minsk, Main str. - 20 , 220205"),
    ("Nike", 335712585, "USA, Boston, Garden str. - 445, 25-457"),
    ("Mike", 257485695, "Poland, Warsaw, Marszałkowska str, 1, 6814-12 ")
]
cursor.executemany(query, values)
database.commit()
# ################################
# CREATE TABLE orders with columns: ---
cursor = database.cursor()
cursor.execute('CREATE TABLE orders ('
               ' id INT(11) NOT NULL AUTO_INCREMENT PRIMARY KEY,'
               ' customerId INT,'
               ' carId INT,'
               ' from_date VARCHAR(30),'
               ' to_date VARCHAR(30),'
               ' FOREIGN KEY(customerId) REFERENCES customers(id),'
               ' FOREIGN KEY(carId) REFERENCES cars(id))')
database.commit()
# -----------------------------
# ADD DATA TO orders: ----------
cursor = database.cursor()
query = "INSERT INTO orders (customerId, carId, from_date, to_date)" \
        " VALUES (%s, %s, %s, %s)"
values = [
    (3, 3, "2015-01-02 19:40", "2015-01-10 14:30"),
    (2, 1, "2019-10-13 17:00", "2019-10-15 23:00"),
    (1, 2, "2020-03-27 14:10", "2020-05-01 12:10"),
    (3, 2, "2021-06-09 08:10", "2021-06-15 16:30")
]
cursor.executemany(query, values)
database.commit()
cursor = database.cursor()
print(cursor.rowcount, "records inserted")
# -----------------------------
#
cursor.execute("DESC orders")
print(cursor.fetchall())
# CLOSE CONNECTION:----------
database.close()
container.stop()




# ************SELECT **************
# database = mysql.connect(host="localhost",
#                          user="dima",
#                          passwd="123",
#                          port=3306,
#                          database="car_sharing"
#                          )
# cursor = database.cursor()
# cursor.execute("SELECT cars.type_car, customers.name, customers.address,"
#                " orders.from_date FROM "
#                "customers INNER JOIN orders ON customers.id=orders.customerId "
#                "INNER JOIN cars ON cars.id=orders.carId ORDER BY orders.id")
# result = cursor.fetchall()
# for row in result:
#     print(row)
# --------------
# cursor.execute("SELECT type_car, car_number FROM cars")
# result = cursor.fetchall()
# for row in result:
#     print(row)
# print()
# --------------
# cursor.execute("SELECT type_car, car_number FROM cars WHERE type_car <> 'jeep'")
# result = cursor.fetchall()
# for row in result:
#     print(row)
# print()
# --------------
# cursor.execute("SELECT type_car, car_number FROM cars WHERE mileage < 50000")
# result = cursor.fetchall()
# for row in result:
#     print(row)
# --------------
# cursor.execute("INSERT INTO cars (type_car, car_number, day_price) "
#                "VALUES "
#                "('cabriolet', '01-05', 200)")
# database.commit()

# cursor.execute("UPDATE cars SET mileage=75400 WHERE id=5")
# database.commit()
# --------------
# cursor.execute("SELECT mileage FROM cars")
# result = cursor.fetchall()
# print(type(result), result)
# mileage = 0
# for miles in result:
#     mileage += miles[0]
# print(mileage)
# --------------

# database.close()

# container.stop()