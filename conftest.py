import xml.etree.cElementTree as ET
from pytest import fixture
from selenium import webdriver
from webdriver_manager.chrome import ChromeDriverManager

@fixture()
def open_browser():
    # browser = webdriver.Chrome()
    browser = webdriver.Chrome(ChromeDriverManager().install())

    yield browser
    browser.close()


@fixture
def get_var_xml_f():
    def variable(tag):
        data = ET.parse("data_base_site.xml")
        root = data.getroot()
        var = root.find(f"variables/{tag}").text
        return var
    return variable
