from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
import xml.etree.ElementTree as ET


class BasePage:
    def __init__(self, browser, url):
        self.browser = browser
        self.url = url
        self.browser.implicitly_wait(5)

    def open(self):
        self.browser.get(self.url)

    def find_wait_element_clickable_return_button(
            self, find_by, locator, timeout=20):
        button = WebDriverWait(self.browser, timeout).until(
            EC.element_to_be_clickable((find_by, locator))
        )
        return button

    def find_wait_element_located_return_element(
            self, find_by, locator, timeout=20):
        element = WebDriverWait(self.browser, timeout).until(
            EC.presence_of_element_located((find_by, locator))
        )
        return element

    @staticmethod
    def get_var_xml(tag):
        data = ET.parse("data_base_site.xml")
        root = data.getroot()
        variable = root.find(f"variables/{tag}").text
        return variable
