from mypages.base_page import BasePage
from selenium.webdriver.common.by import By
from time import sleep


class CartPageLocators:
    LOCATOR_ELEM_TEXT_SUMMARY = (By.ID, "cart_title")
    LOCATOR_ELEM_YOUR_CART = (By.CLASS_NAME, "navigation_page")
    LOCATOR_ELEM_EMPTY = (By.CSS_SELECTOR, "[class='alert alert-warning']")
    # for adding item into cart
    LOCATOR_BUTTON_WOMEN = (By.CSS_SELECTOR, "#block_top_menu ul li"
                                             " [title='Women'].sf-with-ul")
    LOCATOR_ITEM_DRESS = (By.CSS_SELECTOR, "[src='http://automationpractice."
                                           "com/img/p/8/8-home_default.jpg']")
    LOCATOR_BUTTON_ADD_TO_CART = (By.CSS_SELECTOR, "#add_to_cart button")
    LOCATOR_BUTTON_TO_CHECKOUT = (By.CSS_SELECTOR, ".button-container a")
    LOCATOR_TOTAL_PRICE = (By.ID, "total_price")
    LOCATOR_ICON_TRASH = (By.CLASS_NAME, "icon-trash")
    LOCATOR_ICON_MINUS = (By.CLASS_NAME, "icon-minus")


class CartPage(BasePage):
    def should_be_cart_page(self):
        self.should_be_cart_url()
        self.should_be_cart_form()
        self.should_be_cart_element()
        self.should_be_empty()

    def should_be_cart_url(self):
        assert self.browser.current_url == self.get_var_xml("url_cart_page")

    def should_be_cart_form(self):
        # проверка, что есть определенный текст на странице корзины
        element_cart_page = self.find_wait_element_located_return_element(
            *CartPageLocators.LOCATOR_ELEM_TEXT_SUMMARY)
        assert element_cart_page.text == "SHOPPING-CART SUMMARY"

    def should_be_cart_element(self):
        # проверка, что это страница с указателем Your shopping cart
        element_your_cart = self.find_wait_element_located_return_element(
            *CartPageLocators.LOCATOR_ELEM_YOUR_CART)
        assert element_your_cart.text == "Your shopping cart"

    def should_be_empty(self):
        # тест в main_page "test_user_can_open_cart_page проходит без
        # "sleep(2)", а тест "test_user_can_delete_item_from_cart" проходит
        # только со sleep(2) или в дебаге
        # (где-то не хватает времени ожидания) даже 1 сек мало:("
        sleep(2)
        element_empty = self.find_wait_element_located_return_element(
            *CartPageLocators.LOCATOR_ELEM_EMPTY)
        assert element_empty.text == "Your shopping cart is empty."

    def put_item_into_cart(self):
        self.find_wait_element_clickable_return_button(
            *CartPageLocators.LOCATOR_BUTTON_WOMEN).click()
        self.find_wait_element_clickable_return_button(
            *CartPageLocators.LOCATOR_ITEM_DRESS).click()
        self.find_wait_element_clickable_return_button(
            *CartPageLocators.LOCATOR_BUTTON_ADD_TO_CART).click()
        self.find_wait_element_clickable_return_button(
            *CartPageLocators.LOCATOR_BUTTON_TO_CHECKOUT).click()
        total_price = self.find_wait_element_located_return_element(
            *CartPageLocators.LOCATOR_TOTAL_PRICE)
        return total_price

    def put_item_into_cart_and_delete_item_from_cart1(self):
        self.find_wait_element_clickable_return_button(
            *CartPageLocators.LOCATOR_BUTTON_WOMEN).click()
        self.find_wait_element_clickable_return_button(
            *CartPageLocators.LOCATOR_ITEM_DRESS).click()
        self.find_wait_element_clickable_return_button(
            *CartPageLocators.LOCATOR_BUTTON_ADD_TO_CART).click()
        self.find_wait_element_clickable_return_button(
            *CartPageLocators.LOCATOR_BUTTON_TO_CHECKOUT).click()
        self.find_wait_element_clickable_return_button(
            *CartPageLocators.LOCATOR_ICON_TRASH).click()
        self.should_be_empty()
