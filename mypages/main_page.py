from selenium.webdriver.common.by import By
from mypages.base_page import BasePage
from mypages.login_page import LoginPage
from mypages.cart_page import CartPage


class MainPageLocators:
    LOCATOR_LOGIN_BUTTON = (By.CLASS_NAME, "login")
    LOCATOR_CART_BUTTON = (By.CSS_SELECTOR, "div [class='shopping_cart'] a")


class MainPage(BasePage):
    def open_login_page(self):
        button_login_page = self.find_wait_element_clickable_return_button(
            *MainPageLocators.LOCATOR_LOGIN_BUTTON)
        button_login_page.click()
        return LoginPage(self.browser, self.browser.current_url)

    def open_cart_page(self):
        button_cart_page = self.find_wait_element_clickable_return_button(
            *MainPageLocators.LOCATOR_CART_BUTTON)
        button_cart_page.click()
        return CartPage(self.browser, self.browser.current_url)
