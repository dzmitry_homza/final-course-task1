from mypages.base_page import BasePage
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import Select



class LoginPageLocators:
    # for main_page test
    LOCATOR_CREATE_ACCOUNT = (By.CSS_SELECTOR, "[value='Create an account']")
    TEXT_LOGIN_EMAIL = (By.CSS_SELECTOR,
                        "div [class='form-group'] [for='email']")
    FIELD_EMAIL_CREATE_ACCOUNT = (By.CSS_SELECTOR,
                                  "input[type='text'][name='email_create']")
    BUTTON_CREATE_ACCOUNT = (By.ID, "SubmitCreate")
    # for create account test
    LOCATOR_FIELD_GENDER = (By.ID, "id_gender1")
    LOCATOR_FIELD_FIRST_NAME = (By.ID, "customer_firstname")
    LOCATOR_FIELD_LAST_NAME = (By.ID, "customer_lastname")
    LOCATOR_FIELD_PASSWD = (By.ID, "passwd")
    LOCATOR_FIELD_ADDRESS = (By.ID, "address1")
    LOCATOR_FIELD_CITY = (By.ID, "city")
    LOCATOR_FIELD_SELECT_STATE = (By.ID, "id_state")
    LOCATOR_FIELD_ZIP = (By.ID, "postcode")
    LOCATOR_FIELD_MOBILE = (By.ID, "phone_mobile")
    LOCATOR_BUTTON_REGISTER = (By.ID, "submitAccount")
    LOCATOR_ELEMENT_MY_ACC = (By.CLASS_NAME, "info-account")
    # for login
    LOCATOR_FIELD_EMAIL_SIGN_IN = (By.ID, "email")
    LOCATOR_FIELD_PASSWD_SIGN_IN = (By.ID, "passwd")
    LOCATOR_BUTTON__SIGN_IN = (By.ID, "SubmitLogin")
    LOCATOR_ALERT_DANGER = (By.CLASS_NAME, "alert alert-danger")
    LOCATOR_ALERT_DANGER_LI = (By.CSS_SELECTOR, "[class='alert alert-danger'] li")


class LoginPage(BasePage):
    def should_be_login_page(self):
        self.should_be_login_url()
        self.should_be_login_form()
        self.should_be_register_form()

    def should_be_login_url(self):
        """проверка на корректный url адрес"""
        assert self.browser.current_url == self.get_var_xml("url_login_page")

    def should_be_login_form(self):
        """что есть текст рег-ции на странице"""
        element_login_page = self.find_wait_element_located_return_element(
            *LoginPageLocators.TEXT_LOGIN_EMAIL)
        assert element_login_page.text == "Email address"

    def should_be_register_form(self):
        """что есть форма рег-ции на странице (кнопка Create Account)"""
        button_create_account = self.find_wait_element_clickable_return_button(
            *LoginPageLocators.BUTTON_CREATE_ACCOUNT)
        assert button_create_account.get_attribute("type") == "submit"

    def create_account(self):
        """сознание нового аккаунта"""
        # find and fill the field email:
        field_email = self.find_wait_element_located_return_element(
            *LoginPageLocators.FIELD_EMAIL_CREATE_ACCOUNT)
        field_email.send_keys(self.get_var_xml("email"))
        # find and click to the button CREATE ACCOUNT
        self.find_wait_element_clickable_return_button(
            *LoginPageLocators.BUTTON_CREATE_ACCOUNT).click()
        # # fill gender
        self.find_wait_element_clickable_return_button(
            *LoginPageLocators.LOCATOR_FIELD_GENDER).click()
        # fill first name
        self.find_wait_element_clickable_return_button(
            *LoginPageLocators.LOCATOR_FIELD_FIRST_NAME).\
            send_keys(self.get_var_xml("first_name"))

        self.find_wait_element_clickable_return_button(
            *LoginPageLocators.LOCATOR_FIELD_LAST_NAME).\
            send_keys(self.get_var_xml("last_name"))

        self.find_wait_element_clickable_return_button(
            *LoginPageLocators.LOCATOR_FIELD_PASSWD). \
            send_keys(self.get_var_xml("password"))

        self.find_wait_element_clickable_return_button(
            *LoginPageLocators.LOCATOR_FIELD_ADDRESS). \
            send_keys(self.get_var_xml("address"))

        self.find_wait_element_clickable_return_button(
            *LoginPageLocators.LOCATOR_FIELD_CITY). \
            send_keys(self.get_var_xml("city"))

        select = Select(self.browser.find_element_by_id("id_state"))
        select.select_by_visible_text('Wisconsin')

        self.find_wait_element_clickable_return_button(
            *LoginPageLocators.LOCATOR_FIELD_ZIP). \
            send_keys(self.get_var_xml("zip"))

        self.find_wait_element_clickable_return_button(
            *LoginPageLocators.LOCATOR_FIELD_MOBILE). \
            send_keys(self.get_var_xml("mobile_phone"))

        self.find_wait_element_clickable_return_button(
            *LoginPageLocators.LOCATOR_BUTTON_REGISTER).click()
        return self.browser

    def user_sign_in(self):
        """зарегистрированный пользователь может sign in"""
        self.find_wait_element_clickable_return_button(
            *LoginPageLocators.LOCATOR_FIELD_EMAIL_SIGN_IN).\
            send_keys(self.get_var_xml("email"))

        self.find_wait_element_clickable_return_button(
            *LoginPageLocators.LOCATOR_FIELD_PASSWD_SIGN_IN). \
            send_keys(self.get_var_xml("password"))

        self.find_wait_element_clickable_return_button(
            *LoginPageLocators.LOCATOR_BUTTON__SIGN_IN).click()
        return self.browser

    def user_can_not_sign_in_with_bad_email(self):
        self.find_wait_element_clickable_return_button(
            *LoginPageLocators.LOCATOR_FIELD_EMAIL_SIGN_IN). \
            send_keys(self.get_var_xml("bad_email"))

        self.find_wait_element_clickable_return_button(
            *LoginPageLocators.LOCATOR_FIELD_PASSWD_SIGN_IN). \
            send_keys(self.get_var_xml("password"))

        self.find_wait_element_clickable_return_button(
            *LoginPageLocators.LOCATOR_BUTTON__SIGN_IN).click()

        danger = self.find_wait_element_located_return_element(
            *LoginPageLocators.LOCATOR_ALERT_DANGER_LI)
        return danger
