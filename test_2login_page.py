from mypages.login_page import LoginPage

# ! ! ! ЕСЛИ ТЕСТЫ НЕ ЗАПУСКАЮТСЯ-НАДО ИЗМЕНИТЬ email В
#   ФАЙЛЕ create_data_base_XML.py и запустить его
#   (ВОЗМОЖНО ТАКОЙ EMAIL УЖЕ ЗАРЕГИСТРИРОВАН)


def test_user_can_create_account(open_browser, get_var_xml_f):
    login_page = LoginPage(open_browser, get_var_xml_f("url_login_page"))
    login_page.open()
    browser = login_page.create_account()
    assert browser.current_url == LoginPage.get_var_xml("url_my_account")


def test_user_can_sign_in(open_browser, get_var_xml_f):
    login_page = LoginPage(open_browser, get_var_xml_f("url_login_page"))
    login_page.open()
    browser = login_page.user_sign_in()
    assert browser.current_url == LoginPage.get_var_xml("url_my_account")


def test_user_can_not_sign_in_with_bad_email(open_browser, get_var_xml_f):
    login_page = LoginPage(open_browser, get_var_xml_f("url_login_page"))
    login_page.open()
    danger = login_page.user_can_not_sign_in_with_bad_email()
    assert danger.text == LoginPage.get_var_xml("auth_failed")
