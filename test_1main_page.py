from mypages.main_page import MainPage

def test_user_can_open_login_p(open_browser, get_var_xml_f):
    main_page = MainPage(open_browser, get_var_xml_f("base_url"))
    main_page.open()
    login_page = main_page.open_login_page()
    login_page.should_be_login_page()


def test_user_can_open_cart_page(open_browser, get_var_xml_f):
    main_page = MainPage(open_browser, get_var_xml_f("base_url"))
    main_page.open()
    cart_page = main_page.open_cart_page()
    cart_page.should_be_cart_page()
