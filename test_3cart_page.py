from mypages.cart_page import CartPage
# ! ! ! ЕСЛИ ТЕСТЫ НЕ ЗАПУСКАЮТСЯ-НАДО ИЗМЕНИТЬ email В
#   ФАЙЛЕ create_data_base_XML.py и запустить его.
#   (ВОЗМОЖНО ТАКОЙ АДРЕС УЖЕ ЗАРЕГИСТРИРОВАН)


def test_user_can_put_item_in_cart(open_browser, get_var_xml_f):
    """выбрать товар на главной стр, добавить товар в корзину,
    перейти в корзину, проверить сумму корзины"""
    cart_page = CartPage(open_browser, get_var_xml_f("base_url"))
    cart_page.open()
    total_price = cart_page.put_item_into_cart()
    assert total_price.text == get_var_xml_f("total_price")


def test_user_can_delete_item_from_cart(open_browser, get_var_xml_f):
    """выбрать товар на главной стр, добавить товар в корзину,
    перейти в корзину, удалить товар из корзины и проверить"""
    cart_page = CartPage(open_browser, get_var_xml_f("base_url"))
    cart_page.open()
    # пытался здесь применить уже существующий метод "page.should_be_empty()"
    # но тест проходит только в дебаге (где-то не хватает времени ожидания)
    cart_page.put_item_into_cart_and_delete_item_from_cart1()
