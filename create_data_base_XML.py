import xml.etree.cElementTree as ET

root = ET.Element("base")
var = ET.SubElement(root, "variables")

# registration data
ET.SubElement(var, "base_url").text = "http://automationpractice.com/index.php"
ET.SubElement(var, "url_login_page").text = "http://automationpractice.com/index.php?controller=authentication&back=my-account"
ET.SubElement(var, "url_cart_page").text = "http://automationpractice.com/index.php?controller=order"
ET.SubElement(var, "url_my_account").text = "http://automationpractice.com/index.php?controller=my-account"
ET.SubElement(var, "email").text = "gdvprof54@gmail.com"
ET.SubElement(var, "bad_email").text = "qqgdvprof987@gmail.cmm"
ET.SubElement(var, "first_name").text = "Bob"
ET.SubElement(var, "last_name").text = "Dylan"
ET.SubElement(var, "password").text = "12345"
ET.SubElement(var, "address").text = "street Main 200"
ET.SubElement(var, "city").text = "Minsk"
ET.SubElement(var, "zip").text = "22331"
ET.SubElement(var, "mobile_phone").text = "2354456346"
ET.SubElement(var, "text_welcome").text = "Welcome to your account. Here you can manage all of your personal information and orders."
ET.SubElement(var, "auth_failed").text = "Authentication failed."
ET.SubElement(var, "total_price").text = "$28.00"

tree = ET.ElementTree(root)
tree.write("data_base_site.xml")